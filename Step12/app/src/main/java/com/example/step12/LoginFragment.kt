package com.example.step12

import android.accounts.AccountManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentLoginBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

var logPassBase= mutableMapOf<String,String>()
var profileBase = mutableListOf<Profile>()

const val sign=true

class LoginFragment : Fragment() {
    private val dataModel:DataModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {

        val binding = FragmentLoginBinding.inflate(inflater)
        binding.bNavLog.isVisible=false
        logPassBase+="admin" to "admin"
        profileBase.add(Profile("admin","admin","admin","admin","admin"))
        dataModel.person.observe(activity as LifecycleOwner,{
            Log.i("LOGIN", "${it}")
            profileBase.add(it)
            logPassBase+=it.phoneNumber to it.password
        })
        binding.btReg.setOnClickListener{
            val login = binding.etLoginLogin.text.toString()
            val password = binding.etPasswordLogin.text.toString()
            if (login in logPassBase && logPassBase[login]==password)
            {
                for (i in profileBase){
                    Log.i("CHECKING","I WATCH $i")
                    if (login==i.phoneNumber){
                        dataModel.currentProf.value=i
                        Navigation.findNavController(binding.root).navigate(R.id.toHomeFragment)
                        break
                    }
                }
            }
            else{
                Toast.makeText(binding.root.context,"Incorrect login or password",Toast.LENGTH_SHORT).show()
            }
        }
        binding.tvReg.setOnClickListener { Navigation.findNavController(binding.root).navigate(R.id.toRegistrationFragment) }
        return binding.root
    }

    companion object {

        fun newInstance()=LoginFragment()
    }
}