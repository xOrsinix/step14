package com.example.step12

import android.accounts.AccountManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {
    private val dataModel:DataModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentProfileBinding.inflate(inflater)
        binding.bNavProfile.selectedItemId = R.id.profileLabel

        binding.bNavProfile.setOnItemSelectedListener {
            when (it.itemId){
                R.id.profileLabel -> {}

                R.id.webLabel -> {Navigation.findNavController(binding.root).navigate(R.id.fromProfileToWeb)}

                R.id.homeLabel -> {Navigation.findNavController(binding.root).navigate(R.id.fromProfileToHome)}
            }
            true
        }

        dataModel.currentProf.observe(activity as LifecycleOwner,{
            binding.tvNameProfile.text=it.name
            binding.tvPhoneNumberProfile.text=it.phoneNumber
            binding.tvSexProfile.text=it.sex
            binding.tvDateOfBirthProfile.text=it.dateOfBirth
        })

        binding.btLogOut.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.fromProfileToLogin)
        }
        return binding.root
    }

    companion object {
        fun newInstance(param1: String, param2: String) =
            ProfileFragment()
    }
}