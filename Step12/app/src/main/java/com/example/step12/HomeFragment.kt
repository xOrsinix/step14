package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.step12.databinding.FragmentHomeBinding
import com.google.android.material.bottomnavigation.BottomNavigationView


class HomeFragment : Fragment() {
    private val adapter=StringAdapter()

    val strings= arrayListOf<String>("OOO krasevoe", "2+2=5000", "Хочу питсу","Егор Крид <3")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentHomeBinding.inflate(inflater)
        binding.bNavHome.selectedItemId = R.id.homeLabel
        binding.bNavHome.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.homeLabel -> {}

                R.id.webLabel->{
                    Navigation.findNavController(binding.root).navigate(R.id.toWebFragment)
                }

                R.id.profileLabel -> {Navigation.findNavController(binding.root).navigate(R.id.fromHomeToProfile)}
            }
            true
        }

        binding.btAddWord.setOnClickListener {
            adapter.addString(binding.etAddLine.text.toString())
        }


        binding.apply {
            rView.layoutManager=LinearLayoutManager(context)
            rView.adapter=adapter
            adapter.addAll(strings)
        }
        return binding.root
    }


    companion object {

        fun newInstance() = HomeFragment()
    }
}

/*
package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentHomeBinding
import com.google.android.material.bottomnavigation.BottomNavigationView


class HomeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        view.findViewById<BottomNavigationView>(R.id.bNavHome).selectedItemId = R.id.homeLabel
        view.findViewById<BottomNavigationView>(R.id.bNavHome).setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.homeLabel -> {}

                R.id.logLabel -> {
                    Navigation.findNavController(view).navigate(R.id.fromHomeToLogin)
                }

                R.id.webLabel->{
                    Navigation.findNavController(view).navigate(R.id.toWebFragment)
                }
            }
            true
        }
        return view
    }

    companion object {

        fun newInstance() = HomeFragment()
    }
}
 */