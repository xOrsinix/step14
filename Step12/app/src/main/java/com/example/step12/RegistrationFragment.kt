package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentRegistrationBinding
import com.google.android.material.bottomnavigation.BottomNavigationView


class RegistrationFragment : Fragment() {
    private val dataModel:DataModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentRegistrationBinding.inflate(inflater)

        binding.bNavReg.isVisible=false

        binding.btReg.setOnClickListener {
            val name = binding.etName.text.toString()
            val phoneNumber = binding.etPhone.text.toString()
            val date = binding.etDate.text.toString()
            val sex = binding.spinner.selectedItem.toString()
            val password = binding.etPassword.text.toString()
            val prof = Profile(name,phoneNumber,date,sex,password)
            if (name!="" && phoneNumber!="" && date!="" && sex!="" && password!="") {
                dataModel.person.value = prof
                Navigation.findNavController(binding.root).navigate(R.id.toLoginFragment)
            }
            else
                Toast.makeText(binding.root.context,"Please fill in all fields",Toast.LENGTH_SHORT).show()
        }

        return binding.root
    }

    companion object {
        fun newInstance() =
            RegistrationFragment()
    }
}