package com.example.step12

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebResourceRequest
import androidx.fragment.app.Fragment
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.navigation.Navigation
import com.example.step12.databinding.FragmentWebBinding
import com.google.android.material.bottomnavigation.BottomNavigationView



class WebFragment : Fragment() {

    val urlList = arrayListOf<String?>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentWebBinding.inflate(inflater)
        binding.bNavWeb.selectedItemId = R.id.webLabel
        binding.bNavWeb.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.webLabel -> { }

                R.id.profileLabel -> {Navigation.findNavController(binding.root).navigate(R.id.fromWebToProfile)}

                R.id.homeLabel->{
                    Navigation.findNavController(binding.root).navigate(R.id.fromWebToHome)
                }
            }
            true
        }
        val wb = binding.wbV

        val callback = object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                if (wb.canGoBack()){
                    wb.goBack()
                }
                else Navigation.findNavController(binding.root).navigate(R.id.fromWebToHome)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)

        webViewSetup(wb)
        return binding.root
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun webViewSetup(wb:WebView){
        wb.webViewClient= WebViewClient()
        wb.apply {
            loadUrl("https://www.google.com/")
            settings.safeBrowsingEnabled = true
        }

    }

    companion object {
        fun newInstance() = WebFragment()
    }
}