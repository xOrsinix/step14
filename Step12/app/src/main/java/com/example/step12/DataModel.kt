package com.example.step12

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class DataModel:ViewModel() {
    val person:MutableLiveData<Profile> by lazy{
       MutableLiveData<Profile>()
    }

    val currentProf:MutableLiveData<Profile> by lazy{
        MutableLiveData<Profile>()
    }
}